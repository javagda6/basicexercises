package Podstawy.Zadanie24;

import java.util.Scanner;

public class MainAutostradaPrzyklad {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do {
            String liniaZWejsciaTyp = scanner.nextLine();
            CarType typPojazdzu = CarType.valueOf(liniaZWejsciaTyp.toUpperCase());


//            CarType typPojazdzu = CarType.CAR;

            switch (typPojazdzu) {
                case CAR:
                    System.out.println("Wpisales CAR");
                    break;
                case MOTORCYCLE:
                    System.out.println("Wpisales Motorcycle");
                    break;
                case TRUCK:
                    System.out.println("Wpisales TRUCK");
                    break;
            }
        }while(scanner.hasNextLine());
    }
}
