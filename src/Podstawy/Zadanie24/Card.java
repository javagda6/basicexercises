package Podstawy.Zadanie24;

public class Card {
    private CarType typKarty;
    private int number;

    public Card(CarType type, int number) {
        this.typKarty = type;
        this.number = number;
    }

    public CarType getTypKarty() {
        return typKarty;
    }

    public void setTypKarty(CarType typKarty) {
        this.typKarty = typKarty;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
