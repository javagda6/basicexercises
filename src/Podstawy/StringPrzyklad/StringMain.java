package Podstawy.StringPrzyklad;

public class StringMain {
    public static void main(String[] args) {
        String name = null;

        // weryfikacja czy null
        boolean czyNull = name == null;
//        boolean czyNull2 = name.equals(null); // null pointer exception

        boolean czyWartosc = name.equals("wartosc");
        boolean czyWartosc2 = name == "wartosc"; // nope

        // name.equals("");
        // name.equals("") != (name == null)
        // name.equals("") != name.equals(" ");
        // name.equals("") == name.isEmpty()
        boolean czyPusty = name.isEmpty();
        boolean czyPusty2 = name.equals("");
    }
}
