package Podstawy.PrzykładCzas;

import java.time.LocalDateTime;

public class KlasaPrzyklad {
    private LocalDateTime dateTime;

    private KlasaPrzyklad(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public static KlasaPrzyklad stworzDlaMnieObiekt(){
        return new KlasaPrzyklad(LocalDateTime.now());
    }

    @Deprecated
    public static void metoda(){

    }
}
