package Podstawy.PrzykładCzas;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
//        KlasaPrzyklad.metoda(); // przykład metody deprecated (wychodzącej z użytku)

//        Date data = new Date(); // nie stosować
        // data i godzina
        LocalDateTime dataIGodzina = LocalDateTime.now();
        System.out.println(dataIGodzina);

        LocalDateTime localDateTime = LocalDateTime.of(2017, 10, 12, 17, 30);
        System.out.println(localDateTime);

        System.out.println(">>>>>>>");

        // Przechowuje date
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);

        LocalDate localDate1 = LocalDate.of(2017, 10, 12);
        System.out.println(localDate1);


        // Przechowuje czas
        System.out.println(">>>>>>");
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);

        LocalTime time = LocalTime.of(17, 30);
        System.out.println(time);

    }
}
