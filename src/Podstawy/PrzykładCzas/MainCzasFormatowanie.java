package Podstawy.PrzykładCzas;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MainCzasFormatowanie {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        // Formatowanie z daty na tekst (do wypisywania)
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");

        LocalTime localTime = LocalTime.now();

        String sformatowane = localDateTime.format(formatter);
        System.out.println("Godzina & data: " + sformatowane);
        System.out.println("Godzina & data: " + localDateTime);

        String sformatowanaGodzina = localTime.format(formatterTime);
        System.out.println("Godzina & data: " + sformatowanaGodzina);
        System.out.println("Godzina & data: " + localTime);

        System.out.println("<<<<<<<<");
        // Formatowanie z tekstu do obiektu daty
        LocalDateTime localDateTime1 = LocalDateTime.parse("201710-12 17:30", formatter);
        System.out.println(localDateTime1);
    }
}
