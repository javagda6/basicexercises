package Podstawy.Zadanie18;

import java.util.ArrayList;

public class MainZadania18 {

    public static void main(String[] args) {

    }


    ///////// B
    public static long calculateMultiply(ArrayList<Integer> list) {
        long result = 1;
        for (int i = 0; i < list.size(); i++) {
            result *= list.get(i);
        }
        return result;
    }

    public static long calculateMultiply2(ArrayList<Integer> list) {
        long result = 1;
        for (Integer value : list) {
            result *= value;
        }
        return result;
    }
    /////

    ///////// C
    public static double calculateMean(ArrayList<Integer> list) {
        double result = 0;
        for (int i = 0; i < list.size(); i++) {
            result += list.get(i);
        }

        return (result / list.size());
    }

    public static double calculateMean2(ArrayList<Integer> list) {
        double result = 0;
        for (Integer value : list) {
            result += value;
        }
        return (result / list.size());
    }
    /////



}
