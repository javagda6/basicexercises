package Podstawy.Zadanie18;

import java.util.ArrayList;
import java.util.Iterator;

public class MainBoolean {
    public static void main(String[] args) {
        ArrayList<Boolean> booleans = new ArrayList<>();
        booleans.add(true);
        booleans.add(false);
        booleans.add(true);
        booleans.add(true);
        booleans.add(false);
        booleans.add(false);
        booleans.add(true);
        booleans.add(false);
        booleans.add(true);

        System.out.println(booleans);
        for (int i = 0; i < booleans.size(); i++) {
            boolean wartosc = booleans.get(i);

            booleans.remove(i);
            booleans.add(i, !wartosc);
        }
        System.out.println(booleans);
//
//        Iterator<Boolean> iterator = booleans.iterator();
//        int licznik = 0;
//        while(iterator.hasNext()){
//            // pobranie nastepnego elementu
//            // uwaga! wywolanie kolejnego 'next' powoduje przejscie do nastepnego elementu
//            boolean wartosc = iterator.next();
//
//            // usuniecie ostatnio pobranego przez metodę 'next' elementu
//            iterator.remove();
//
//            booleans.add(licznik++, !wartosc);
//        }

//        int licznik = 0;
//        for (Boolean wartosc : booleans) {
//            boolean wartoscTmp = wartosc;
//
//            booleans.remove(licznik); // booleans.reamove(i);
//            booleans.add(licznik++, !wartoscTmp);
//        }
//        System.out.println(booleans);

        ArrayList<Boolean> rewritten = new ArrayList<>();
        for (Boolean wartosc : booleans) {
            rewritten.add(!wartosc);
        }
        System.out.println(rewritten);


        ///////// Koniunkcja
        boolean resultKoniunkcja = true;

        for (Boolean wartosc : booleans) {
            resultKoniunkcja = resultKoniunkcja && wartosc;
        }
        System.out.println("Koniunkcja: " + resultKoniunkcja);

        ///////// Alternatywa

        boolean resultAlternatywa = false;

        for (Boolean wartosc : booleans) {
            resultAlternatywa = resultAlternatywa || wartosc;
        }
        System.out.println("Koniunkcja: " + resultAlternatywa);
    }
}
