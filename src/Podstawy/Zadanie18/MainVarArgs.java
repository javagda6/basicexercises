package Podstawy.Zadanie18;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainVarArgs {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>();

        dodajWszystkieDoListy(lista, 1, 2, 3, 4, 5, 6,7 ,8);


        dodajWszystkieDoListy(lista, 100, 23123, 123123);
    }

    
    public static void dodajWszystkieDoListy(ArrayList<Integer> lista, Integer... tablica){
        for (Integer liczbaWTablicy : tablica ) {
            lista.add(liczbaWTablicy);
        }
    }
}
