package Podstawy.Zadanie18;

import java.util.ArrayList;

public class MainReturnProperly {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList();
        list.add(1);
        list.add(5);
        list.add(8);
        list.add(2);

        // metoda zwraca wartość
        int suma = calculateTheSumofTheList(list);

        // wypisuje wartość
        System.out.println("Suma liczb na liscie: " + suma);
    }

    public static int calculateTheSumofTheList(ArrayList<Integer> list) {
        int suma = 0;
        for (int i = 0; i < list.size(); i++) {
            suma += list.get(i);
        }

        return suma;
    }
}
