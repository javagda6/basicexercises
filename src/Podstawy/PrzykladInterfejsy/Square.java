package Podstawy.PrzykladInterfejsy;

public class Square extends Printable implements IFigure, IPrintable{
    @Override
    public double calculateArea() {
        return 0;
    }

    @Override
    public double calculatePerim() {
        return 0;
    }

    @Override
    public void printMe() {
        System.out.println("Print me");
    }
}
