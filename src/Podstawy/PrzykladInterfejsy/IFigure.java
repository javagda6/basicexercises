package Podstawy.PrzykladInterfejsy;

public interface IFigure {
//    protected double field;

    double calculateArea();
    double calculatePerim();

//    public void printSomething(){
//        System.out.println("Something");
//    }
}
