package Podstawy.Zadanie14;

public class MainZadanie14 {
    public static void main(String[] args) {
        Paczka paczka = new Paczka("Odbiorca", true);
        Paczka p = new Paczka();

//        paczka.wyslij();
        paczka.wyslijPolecony();

//        p.wyslij();
        p.wyslij();
        p.wyslij();
        p.wyslij();
    }
}
