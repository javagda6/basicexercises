package Podstawy.Zadanie30;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class MainZadanie30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // podstawowo
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        String line;
        do{ // komercyjnie - nie róbcie tak
            line = sc.nextLine(); // String - linia z wejscia
            if(line.equals("quit")) break;
            try{
                // sparsuj linie wpisana przez uzytkownika
                // poslugujac sie formatterem
                LocalDateTime zmienna = LocalDateTime.parse(line, formatter);

                // jesli uda sie
                System.out.println("OK!"); // jeśli będzie ok, wypisze sie komunikat
            }catch (DateTimeParseException dtpe){
                System.out.println("Niepoprawny format daty");
            }

        }while(!line.equals("quit"));
    }
}
