package Podstawy.Zadanie30;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class MainZadanie30Exception {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // podstawowo
        System.out.println("Podaj format daty:");
        String format = sc.nextLine();
        //
        DateTimeFormatter formatter ;
        try {
            formatter = DateTimeFormatter.ofPattern(format);
        }catch (IllegalArgumentException iae){
            return;
        }

        String line;
        do{ // komercyjnie - nie róbcie tak
            line = sc.nextLine(); // String - linia z wejscia
            if(line.equals("quit")) break;
            try {
                try {
                    // sparsuj linie wpisana przez uzytkownika
                    // poslugujac sie formatterem
                    LocalDateTime zmienna = LocalDateTime.parse(line, formatter);

                    // jesli uda sie
                    System.out.println("OK!"); // jeśli będzie ok, wypisze sie komunikat
                } catch (DateTimeParseException dtpe) {
                    System.out.println("Lapie exception DateTimeParseException");
//                    throw new WrongDateTimeException();
                }
            }catch (WrongDateTimeException wdte){           // z polecenia wrong date time exception
                System.out.println("Niepoprawny format daty");
                System.out.println(wdte.getMessage());
            }

        }while(!line.equals("quit"));

//        if(addTwoNumbers(2, 3) == 5){
//            System.out.println("test przeszedl pomyslnie");
//        }
    }
//
//    public static long addTwoNumbers(int a, int b){
//        return a+b;
//    }


    public static void metoda1(){
        //

        for (int i = 0; i < 10; i++) {

            if( i== 5){
                return;
            }
        }
        //
    }
}
