package Podstawy.Zadanie31;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MainZadanie31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        while(sc.hasNextLine()){
            sc.nextLine();
            LocalDateTime localDateTime = LocalDateTime.now();
            String sformatowane = localDateTime.format(formatter);
            System.out.println("Data & Godzina: " +sformatowane);
        }
    }
}
