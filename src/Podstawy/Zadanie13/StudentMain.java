package Podstawy.Zadanie13;

public class StudentMain {
    public static void main(String[] args) {
        Student student = new Student("Imie", "123123");
        student.addGrade(5);
        student.addGrade(4);
        student.addGrade(3);
        student.addGrade(2);
        student.addGrade(1);

        System.out.println();
        System.out.println("Srednia: " + student.calculateMean());
        System.out.println();
        System.out.println("Ilosc niedostatecznych: " + student.countLowGrades());
        System.out.println();
        System.out.println("Zdalem: " + student.didIPass());
        System.out.println();

        Student student2 = new Student("Imie2", "123123");
        student2.addGrade(5);
        student2.addGrade(4);
        student2.addGrade(4);
        student2.addGrade(3);
        student2.addGrade(3);


        System.out.println();
        System.out.println("Srednia: " + student2.calculateMean());
        System.out.println();
        System.out.println("Ilosc niedostatecznych: " + student2.countLowGrades());
        System.out.println();
        System.out.println("Zdalem: " + student2.didIPass());
        System.out.println();
    }
}
