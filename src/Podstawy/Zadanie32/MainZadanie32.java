package Podstawy.Zadanie32;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MainZadanie32 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;
        do {
            // wczytuje linie
            String line = scanner.nextLine();

            // zamieniam na same male znaki i bez bialych znakow na
            // poczatku i koncu
            String lowerCaseTrimmed = line.toLowerCase().trim();
            DateTimeFormatter dtf;
            switch (lowerCaseTrimmed) {
                case "date":
                    dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    System.out.println(LocalDate.now().format(dtf));
                    break;
                case "time":
                    dtf = DateTimeFormatter.ofPattern("HH:mm");
                    System.out.println(LocalTime.now().format(dtf));
                    break;
                case "datetime":
                    dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                    System.out.println(LocalDateTime.now().format(dtf));
                    break;
                case "quit":
                    isWorking = false;
                    break;
                default:
                    System.out.println("Unknown command");
            }

        } while (isWorking);
    }
}
