package Podstawy.Zadanie9;

public class Main {
    public static void main(String[] args) {
        // nie w kontekście statycznym
        MyMath obiekt = new MyMath();
        System.out.println("ABS: z -5 : " + obiekt.abs(-5) );

        // w kontekście statycznym
        System.out.println("ABS: z -5 : " + MyMathStatic.abs(-5) );
//
//        for (int i = 0; i <= 5; i++) {
//            System.out.println("obieg");
//        }
    }
}
