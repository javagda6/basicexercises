package Podstawy;

import Podstawy.Zadanie26.NoSuchStudentException;
import Podstawy.Zadanie26.Student;
import Podstawy.Zadanie26.StudentFormat;
import Podstawy.Zadanie26.University;

public class PrzykładThrow {
    public static void main(String[] args) {
        try {
            metoda1();
        } catch (Throwable nsse) {
            System.out.println("Rzuciles, przechwycilem");
        }

    }

    public static void printStudentUsing(StudentFormat format, Student st) {
        format.format(st);
    }

    public static void metoda1() {
        metoda2();
    }

    public static void metoda2() {
        try {
            metoda3();
        } catch (NoSuchStudentException nsse) {
            System.out.println("rzucone i jestem w metodzie dwa, przechwycilem");
//            throw nsse; // odkomentuj mnie :))
        }
    }

    public static void metoda3() {
        University university = new University();
//        try {
        Student st = university.getStudent(100201L);
//        } catch (RuntimeException nsse) {

//        }
    }
}
