package Podstawy.Zadanie25;

import java.util.HashSet;
import java.util.Set;

public class MainFG {
    public static void main(String[] args) {
        Set<ParaLiczb> zbior = new HashSet<>();
        zbior.add(new ParaLiczb(1, 2));
        zbior.add(new ParaLiczb(2, 2));
        zbior.add(new ParaLiczb(2, 1));
        zbior.add(new ParaLiczb(1, 2));

        // spodziewamy sie usuniecia duplikatów
        System.out.println(zbior);
    }
}
