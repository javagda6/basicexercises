package Podstawy.Zadanie25;

import java.util.*;

public class Main {
    public static void main(String[] args) {

    }

    // sprawdzanie duplikatów znaku w słowie/zdaniu
    public static boolean containsDuplicates(String text) {
        // pozbywam się kropek, przecinków oraz spacji ze słowa
        // a następnie trim - usuwam białe znaki (spacje i taby)
        // przed i po zdaniu/słowie
        String trimmed = text.replace(",", "")
                .replace(".", "")
                .replace(" ", "").trim();

        // zamieniam ciąg znaków (String) na tablicę znaków
        char[] charArray = trimmed.toCharArray();

        // tworzę listę do której będę umieszczał wszystkie litery
        // (wraz z duplikatami)
        List<Character> listOfChars = new ArrayList<>();
        for (char c : charArray) {
            // dodaje każdy znak z tablicy do listy
            listOfChars.add(c);
        }
        // umieszczam wszytkie znaki z listy do zbioru
        // w zbiorze nie moze byc duplikatow wiec wszystkie znaki
        // zduplikowane zostana usuniete
        Set<Character> zbior = new HashSet<>(listOfChars);

        // z tego wzgledu (usuniecia duplikatow) rozmiar listy i zbioru
        // bedzie sie od siebie roznic. Jesli sa rowne, to znaczy ze nie
        // ma duplikatu
        return zbior.size() != listOfChars.size();
    }

    // Metoda zlicza i wyświetla litery które są
    // zduplikowane w słowie wpisanym
    public static void printDuplicates(String text) {
        String trimmed = text.replace(",", "")
                .replace(".", "")
                .replace(" ", "").trim();
        char[] charArray = trimmed.toCharArray();

        List<Character> listOfChars = new ArrayList<>();
        // tworze mape, kluczem jest litera, a wartoscią jest
        // ilość jej wystąpień w słowie
        Map<Character, Integer> map = new HashMap<>();
        for (char c : charArray) {
            listOfChars.add(c);
            // umieszczam litere w liście

            // zwiększam licznik w mapie
            if (map.containsKey(c)) { // jeśli w mapie jest już
                // licznik danej litery (c)
                // to pobieram go
                Integer ileWystapienJuzJest = map.get(c); // pobranie ilości wystąpień
                map.put(c, ileWystapienJuzJest + 1); // wstawienie do mapy (nadpisanie)
                // ilości wystąpień zwiększonej o 1
            } else {
                map.put(c, 1); // jeśli nie ma jeszcze wystąpienia w mapie, to wstawiam 1
            }
        }

        // dla każdego wpisu (para litera=>ilość_wystąpień)
        // wypisuje tylko litery których ilość wystąpień jest wyższa
        // lub równa 2
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() >= 2) {
                System.out.println(entry.getKey()); // wypisanie
            }
        }
//        return zbior.size() == listOfChars.size();
    }

    public static void printDuplicates2(String text) {
        String trimmed = text.replace(",", "")
                .replace(".", "")
                .replace(" ", "").trim();
        char[] charArray = trimmed.toCharArray();

//        List<Character> listOfChars = new ArrayList<>();
//        List<Character> list = Arrays.asList(charArray);
    }


}
