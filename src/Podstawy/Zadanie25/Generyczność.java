package Podstawy.Zadanie25;

import Podstawy.Zadanie17.Person;

import java.util.HashSet;
import java.util.Set;

public class Generyczność {

    public static void main(String[] args) {

        Set<Double> wynik = metodaNewSet(2.0, 1.0, 5.0, 11.0, 23.0, 124.123, 123123.534);

        Set<Float> wynik2 = metodaNewSet();
    }

//    public static Set<Double> metodaNewSet(double... wartosci) {
//        Set<Double> set = new HashSet<>();
//        for (double wartosc : wartosci) {
//            set.add(wartosc);
//        }
//        if (wartosci.length != set.size()) {
//            throw new IllegalArgumentException("Są duplikaty");
//        }
//        return set;
//    }

    public static <T> Set<T> metodaNewSet(T... wartosci) {
        Set<T> set = new HashSet<>();
        for (T wartosc : wartosci) {
            set.add(wartosc);
        }
        if (wartosci.length != set.size()) {
            throw new IllegalArgumentException("Są duplikaty");
        }
        return set;
    }
}
