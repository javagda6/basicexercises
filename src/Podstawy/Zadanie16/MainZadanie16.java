package Podstawy.Zadanie16;

public class MainZadanie16 {
    public static void main(String[] args) {
        Computer computer1 = new Computer("name", 3.0, 100, 16);
        computer1.setGPUName("enwidia");

        System.out.println(computer1.getProcessingPower());
        System.out.println(computer1.isForGames());
    }
}
