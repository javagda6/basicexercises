package Podstawy.Zadanie12;

import Podstawy.Zadanie7.Circle;
import Podstawy.Zadanie7.Rectangle;
import Podstawy.Zadanie7.Square;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Command format: " +
                    "count area/perimeter square/circle/rectangle X [Y]");
            String command = scanner.nextLine();

            command = command.toLowerCase().trim();
            if (command.startsWith("quit")) {
                System.out.println("Quitting");
                break;
            }

            // dzielimy komende na slowa:
            String[] words = command.split(" ");

            try {
                String countWord = words[0];
                String calculation = words[1];
                String shape = words[2];

                if (!countWord.equals("count")) {
                    continue;
                }

                boolean isArea = calculation.equals("area");
                boolean isPerimeter = calculation.equals("perimeter");

//                if (shape.equals("square")) {
//                    handleSquare(words[3], isArea, isPerimeter);
//                } else if (shape.equals("rectangle")) {
//                    handleRectangle(words, isArea, isPerimeter);
//                } else if (shape.equals("circle")) {
//                    handleCircle(words[3], isArea, isPerimeter);
//                }else{
//
//                }
                
                switch (shape) {
                    case "square":
                        Main.handleSquare(words[3], isArea, isPerimeter);
                        break;
                    case "rectangle":
                        Main.handleRectangle(words, isArea, isPerimeter);
                        break;
                    case "circle":
                        Main.handleCircle(words[3], isArea, isPerimeter);
                        break;
                    case "":
                    case " ":

                    default:

                }


            } catch (ArrayIndexOutOfBoundsException aioobe) {
                // wykroczeznie poza tablice
                System.out.println("Wrong command format. Cot enough parameters");
            } catch (NumberFormatException nfe) {
                System.out.println("Wrong command format. NaN value passed as number.");
            }
        } while (scanner.hasNextLine());

    }

    private static void handleCircle(String word, boolean isArea, boolean isPerimeter) {
        String radiusLength = word;

        int radiusLengthAsInt = Integer.parseInt(radiusLength);

        Circle circle = new Circle(radiusLengthAsInt);
        if (isArea) {
            System.out.println(circle.calculateArea());
        } else if (isPerimeter) {
            System.out.println(circle.calculatePerimeter());
        } else {
            System.out.println("Calculation command error");
        }
    }

    private static void handleRectangle(String[] words, boolean isArea, boolean isPerimeter) {
        String sideLengthA = words[3];
        String sideLengthB = words[4];

        int sideLengthAAsInt = Integer.parseInt(sideLengthA);
        int sideLengthBAsInt = Integer.parseInt(sideLengthB);

        Rectangle rectangle = new Rectangle(sideLengthAAsInt, sideLengthBAsInt);
        if (isArea) {
            System.out.println(rectangle.calculateArea());
        } else if (isPerimeter) {
            System.out.println(rectangle.calculatePerimeter());
        } else {
            System.out.println("Calculation command error");
        }
    }

    private static void handleSquare(String word, boolean isArea, boolean isPerimeter) {
        String sideLength = word;

        int sideLengthAsInt = Integer.parseInt(sideLength);

        Square square = new Square(sideLengthAsInt);
        if (isArea) {
            System.out.println(square.calculateArea());
        } else if (isPerimeter) {
            System.out.println(square.calculatePerimeter());
        } else {
            System.out.println("Calculation command error");
        }
    }
}
