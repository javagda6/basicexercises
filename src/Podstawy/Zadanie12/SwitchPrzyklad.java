package Podstawy.Zadanie12;

import java.util.Scanner;

public class SwitchPrzyklad {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        do {
            String line = sc.nextLine();

            int liczba = 0;
            switch (liczba){
                case 1:
                    break;
                case 2:
                    break;
            }

            switch (line) {
                case "motocykl":
                case "osobowy":
                    System.out.println("Wpisales 2");
                    break;
                case "3":
                    System.out.println("Wpisales 3");
                    break;
                case "quit":
                    System.out.println("Wpisales quit");
                    break;
                default:
                    System.out.println("Wpisales cos innego");
            }
        } while (sc.hasNextLine());
    }
}
