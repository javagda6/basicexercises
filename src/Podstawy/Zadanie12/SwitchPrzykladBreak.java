package Podstawy.Zadanie12;

import java.util.Scanner;

public class SwitchPrzykladBreak {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isRunning = true;
        do {
            String line = sc.nextLine();

            switch (line) {
                case "cos":
                    break;
                case "quit":
                    isRunning = false;
                    break;
                default:
                    break;

            }

        } while (isRunning);
    }
}
