package Podstawy.Zadanie19.BDStudenci;

import Podstawy.Zadanie13.Student;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class StudentDatabase {
    private List<Student> listaStudentow;

    public StudentDatabase() {
        listaStudentow = new LinkedList<>();
    }

    public void createNewList(){
        listaStudentow = new LinkedList<>();
    }

    public void addStudent(Student a) {
        listaStudentow.add(a);
    }

    public void removeStudent(Student s) {
        listaStudentow.remove(s);
    }
}
