package Podstawy.Zadanie19.Family;

import Podstawy.Zadanie19.Family.Person;

public class Mother extends Person {
    private int height;

    public Mother(String name, String surname, int height) {
        super(name, surname);
        this.height = height;
    }

    @Override
    public void printWhoIAm() {
//        super.printWhoIAm();
        System.out.println("I am your mother, my name is " + name);
    }

    public void metodaMatki() {
        System.out.println("Jestem mamuśka");
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean isParent() {
        return true;
    }
}
