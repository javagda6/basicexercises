package Podstawy.Zadanie19.Family;

public abstract class Person {
    protected String name;
    protected String surname;
    protected int age;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void printWhoIAm(){
        System.out.println("I am person, dunno.");
        jakasMetoda();
    }

    public abstract boolean isParent();

    private void jakasMetoda(){
        System.out.println("abc");
    }
}
