package Podstawy.Zadanie19.Family;

public class ZUS {

    public void handlePerson(Person m ){
        m.printWhoIAm();

        // obiekt klasy nadrzędnej m
        if(m instanceof Child){
            Child innaZmienna = (Child) m;

            innaZmienna.playWithStick();
        }else if (m instanceof Mother){
            Mother innaZmienna = (Mother) m;

            innaZmienna.metodaMatki();
        }
    }
}
