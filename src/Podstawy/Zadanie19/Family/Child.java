package Podstawy.Zadanie19.Family;

public class Child extends Person {

    public Child(String name, String surname) {
        super(name, surname);
    }

    @Override
    public void printWhoIAm() {
        System.out.println("I am child: " + getName());
//        super.printWhoIAm();
    }

    public void playWithStick(){
        System.out.println("I am playing");
    }

    @Override
    public boolean isParent(){
        return false;
    }
}
