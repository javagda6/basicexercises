package Podstawy.Zadanie8;

public class BankAccount {
    private double money;

    public BankAccount() {
    }

    public BankAccount(double money) {
        this.money = money;
    }

    public void addMoney(/*Parametr*/ double howMuchToAdd) {
        money = money + howMuchToAdd;
    }

    public void subMoney(/*Parametr*/ double howMuchToSubtract) {
        money = money - howMuchToSubtract;
    }

    public double getMoney(){
        return money;
    }

}
