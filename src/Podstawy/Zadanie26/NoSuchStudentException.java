package Podstawy.Zadanie26;

public class NoSuchStudentException extends RuntimeException{
    public NoSuchStudentException(String message) {
        super(message);
    }
}
