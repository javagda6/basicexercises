package Podstawy.Zadanie26;

import java.util.*;

public class MainStudenciDoPunktuH {
    public static void main(String[] args) {
//
//        Map<Long, String> mapa = new HashMap<>(); // indeks = > nazwisko
//        Map<String, Long> mapa2 = new HashMap<>(); // nazwisko = > indeks
//
//        Student X = new Student(123L, "a", "b");
//        mapa.put(X.getIndeks(), X.getNazwisko());
//        mapa2.put(X.getNazwisko(), X.getIndeks());

        Map<Long, Student> mapa = new HashMap<>();
        List<Student> lista = new ArrayList<>();
        Student sa = new Student(123123L, "a", "A");
        Student sb = new Student(100200L, "b", "B");
        Student sc = new Student(123125L, "c", "C");
        Student sd = new Student(123126L, "d", "D");
        Student se = new Student(123127L, "e", "E");

        mapa.put(sa.getIndeks(), sa);
        mapa.put(sb.getIndeks(), sb);
        mapa.put(sc.getIndeks(), sc);
        mapa.put(sd.getIndeks(), sd);
        mapa.put(se.getIndeks(), se);

        lista.add(sa);
        lista.add(sb);
        lista.add(sc);
        lista.add(sd);
        lista.add(se);

        System.out.println("Klucze (indeksy):");
        for (Long indeks : mapa.keySet()) {
            System.out.println(indeks);
        }

        System.out.println("Wartości (obiekty Student):");
        for (Student student : mapa.values()) {
            System.out.println(student);
        }

        // chce sprawdzić czy istnieje w mapie student o indeksie 100200
        System.out.println("Istnieje: " + mapa.containsKey(100201L));

        System.out.println("Istnieje sc: " + mapa.containsValue(sc));

        for (Student stu : lista) {
            if (stu.getIndeks() == 100200L) {
                System.out.println("Znalazłem, istnieje");
                break;
            }
        }

        Student szukany = mapa.get(100201L);
        if(szukany != null) {
            System.out.println("Szukany:" + szukany.getIndeks());
        }
    }
}
