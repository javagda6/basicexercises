package Podstawy.Zadanie17;

public class MainZadanie17 {
    public static void main(String[] args) {

        Club c = new Club();
        Person p1 = new Person(16);
        Person p2 = new Person(20);

        try {
            c.enter(p1);
        } catch (NoAdultException e) {
            e.printStackTrace();
        }


        /// popróbować
//        throw new RuntimeException();

//        try {
//            throw new Exception();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
