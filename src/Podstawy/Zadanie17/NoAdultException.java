package Podstawy.Zadanie17;

public class NoAdultException extends RuntimeException {

    public NoAdultException() {
        super("Nie wpuszczamy nieletnich");
    }
}
