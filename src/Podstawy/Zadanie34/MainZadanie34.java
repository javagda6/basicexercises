package Podstawy.Zadanie34;

import java.util.Scanner;

public class MainZadanie34 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        do {
            String line = sc.nextLine();

            String upperTrimmed = line.toUpperCase().trim();
            switch (upperTrimmed) {
                case "QUIT":
                    isWorking = false;
                    break;
                default:
                    try {
                        Plec plec = Plec.valueOf(upperTrimmed);
                        System.out.println("Wpisano: " + plec);
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Niepoprawna wartość: " + upperTrimmed);
                    }
            }
        } while (isWorking);
    }

    public static Plec changeToPlec(String slowo) {
        if (slowo.toUpperCase().trim().equals("KOBIETA")) {
            return Plec.KOBIETA;
        } else if (slowo.toUpperCase().trim().equals("MEZCZYZNA")) {
            return Plec.MEZCZYZNA;
        }
        // !! nie dodajemy specjalnie enuma
        return Plec.UNKNOWN; // #zło
    }
}
