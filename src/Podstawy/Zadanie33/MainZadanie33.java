package Podstawy.Zadanie33;

public class MainZadanie33 {
    public static void main(String[] args) {
        new Dog().makeNoise();

        new Cat().makeNoise();

        Cat c = new Cat();
        c.makeNoise();

        Animal animal = new Dog();
        animal.makeNoise();
    }
}
