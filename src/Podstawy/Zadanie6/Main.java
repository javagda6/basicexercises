package Podstawy.Zadanie6;

public class Main {
    public static void main(String[] args) {
        QuadraticEquation equation = new QuadraticEquation(-2, 3, -1);
        System.out.println("Delta: " +equation.calculateDelta());
        System.out.println("X1: " +equation.calculateX1());
        System.out.println("X2: " +equation.calculateX2());
    }
}
