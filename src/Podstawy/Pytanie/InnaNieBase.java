package Podstawy.Pytanie;

public class InnaNieBase extends Base {
    private String inne;

    public InnaNieBase(String pole) {
        super(pole);
    }

    public InnaNieBase(String pole, String inne) {
        super(pole);
        this.inne = inne;
    }
}
