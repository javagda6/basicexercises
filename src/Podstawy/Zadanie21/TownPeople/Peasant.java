package Podstawy.Zadanie21.TownPeople;

public class Peasant extends Citizen {
    public Peasant(String name) {
        super(name);
    }

    @Override
    public boolean canVote() {
        return false;
    }
}
