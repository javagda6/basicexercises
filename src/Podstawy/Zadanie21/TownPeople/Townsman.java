package Podstawy.Zadanie21.TownPeople;

public class Townsman extends Citizen {
    public Townsman(String name) {
        super(name);
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
