package Podstawy.Zadanie21;

import Podstawy.Zadanie21.TownPeople.*;

public class MainZadanie21 {
    public static void main(String[] args) {
        Town miato = new Town();

        miato.addCitizen(new King("król"));
        miato.addCitizen(new Townsman("townsman1"));
        miato.addCitizen(new Townsman("townsman2"));
        miato.addCitizen(new Townsman("townsman3"));
        miato.addCitizen(new Townsman("townsman4"));
        miato.addCitizen(new Townsman("townsman5"));
        miato.addCitizen(new Townsman("townsman6"));
        miato.addCitizen(new Townsman("townsman7"));
        miato.addCitizen(new Soldier("sol2"));
        miato.addCitizen(new Soldier("sol1"));
        miato.addCitizen(new Peasant("Peasant2"));
        miato.addCitizen(new Peasant("Peasant1"));

        System.out.println("Ilosc obywateli mogacych glosowac:" + miato.howManyCanVote());
        System.out.println("Kto moze glosowac: " + miato.whoCanVote());
        miato.whoCanVote2();
//        System.out.println("Kto moze glosowac: " + miato.whoCanVote2());
    }
}
